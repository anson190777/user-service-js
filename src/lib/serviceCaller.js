const generateSafeId = require('generate-safe-id');
const jwtHelper = require('../lib/jwtHelper');
const keys = require('../lib/keys');

const getUUID = () => Promise.resolve(generateSafeId());

const generateJWT = data =>
  new Promise(async (resolve, reject) => {
    try {
      const result = jwtHelper.signJwt(data, keys.getJwtPrivateKey());

      return resolve(result);
    } catch (error) {
      return reject(error);
    }
  });

module.exports = { getUUID, generateJWT };
