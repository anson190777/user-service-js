const fs = require('fs');
const path = require('path');
const config = require('config');

const keys = {};

const load = () => {
  //read private key rsa
  keys.jwtPrivateKey = fs.readFileSync(
    path.resolve(config.get('jwtPrivateKeyPath')),
    'utf8',
  );

  //read public key rsa
  keys.jwtPublicKey = fs.readFileSync(
    path.resolve(config.get('jwtPublicKeyPath')),
    'utf8',
  );

  // eslint-disable-next-line
  console.log('keys is loaded');
};

module.exports = {
  load,
  getJwtPrivateKey: () => keys.jwtPrivateKey,
  getJwtPublicKey: () => keys.jwtPublicKey,
};
