const jwt = require('jsonwebtoken');

module.exports.decode = token => {
  const decoded = jwt.decode(token);
  return decoded;
};
