const jwt = require('jsonwebtoken');

// TODO: add exp to jwt token
//generate token by payload and private key
module.exports.signJwt = (payload, privateKey) =>
  new Promise((resolve, reject) => {
    jwt.sign(
      { ...payload, exp: Date.now(), iss: 'comspaces' },
      privateKey,
      { algorithm: 'RS256' },
      (err, token) => {
        if (err) {
          reject(err);
        } else {
          resolve(token);
        }
      }
    );
  });

//decode token into payload
module.exports.verifyJwt = (token, privKey) =>
  new Promise((resolve, reject) => {
    jwt.verify(token, privKey, { algorithms: 'RS256' }, (err, decoded) => {
      if (err) {
        reject(err);
      } else {
        resolve(decoded);
      }
    });
  });
