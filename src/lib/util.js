const crypto = require('crypto');
// const jwt = require('jsonwebtoken');
const randomstring = require('randomstring');
const createError = require('http-errors');
const bcrypt = require('./bcrypt');

//generate token
module.exports.encryptStringWithRsaPublicKey = (pubkey, toEncrypt) =>
  // eslint-disable-next-line
  new Promise((resolve, reject) => {
    const buffer = Buffer.from(toEncrypt);
    const encrypted = crypto.publicEncrypt(pubkey, buffer);
    return resolve(encrypted.toString('base64'));
  });

/**
 * @param {string} privateKey - the string that content private key to descypt the encrypted password
 * @param {string} encrypted - the encrypted content that need to decrypt
 * @returns {Promise} - Promise object represents to the content that being descrypted
 */
module.exports.decryptStringWithRsaPrivateKey = (pivkey, toDecrypt) =>
  // eslint-disable-next-line
  new Promise((resolve, reject) => {
    try {
      const buffer = Buffer.from(toDecrypt, 'base64');
      const decrypted = crypto.privateDecrypt(pivkey, buffer);
      const password = decrypted.toString('utf8');
      return resolve(password);
    } catch (error) {
      return reject(
        createError(500, 'unable to descrypt password', {
          errors: [{ code: 500, message: 'unable to descrypt password' }],
        })
      );
    }
  });

module.exports.bcryptCompare = (value, encrypted) =>
  new Promise((resolve, reject) => {
    bcrypt.compare(value, encrypted, (err, same) => {
      if (err) {
        return reject(err);
      }

      if (!same) {
        return resolve(false);
      }
      return resolve(true);
    });
  });

module.exports.generateRandomCode = () =>
  Promise.resolve(
    randomstring.generate({
      length: 6,
      charset: 'numeric',
    })
  );
