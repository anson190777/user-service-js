const Result = require('folktale/result');
const R = require('ramda');
const config = require('config');
const userSrv = require('../services/user.service');
// const util = require('../../lib/util');
// const keys = require('../../lib/keys');
const bcrypt = require('../../lib/bcrypt');
const serviceCaller = require('../../lib/serviceCaller');
const errors = require('../errors');

module.exports = async ctx => {
  try {
    const { name, email, password } = ctx.request.body;

    // hash password
    const hashPwd = await bcrypt.hash(
      password,
      parseInt(config.get('bcrypt.saltRounds'), 10)
    );

    // get uuid
    const uuid = await serviceCaller.getUUID();
    if (uuid.value instanceof Error) throw uuid.value;

    // create verification code
    // const verifyCode = await util.generateRandomCode();

    const resultFindEmail = await userSrv.findOne({ email: email });

    if (resultFindEmail.value instanceof Error) throw resultFindEmail.value;

    if (resultFindEmail.value) throw errors.UserPostAlreadyExist();

    const registerResult = await userSrv.insertOne({
      _id: uuid,
      name,
      email,
      password: hashPwd,
    });

    if (registerResult.value instanceof Error) throw registerResult.value;

    return Promise.resolve(
      Result.Ok({
        status: 201,
        body: {
          user: {
            id: registerResult.value._id,
            ...R.omit(['password', '_id'], registerResult.value),
          },
        },
      })
    );
  } catch (error) {
    return Promise.resolve(Result.Error(error));
  }
};
