const Router = require('koa-router');
const schema = require('./schema');
const handler = require('./handler');
const validator = require('../middlewares/validator');
const Result = require('folktale/result');

const router = new Router();

router.post('/register', validator(schema.register), handler.register);

router.get('/', function(ctx) {
  ctx.response.status = 200;
  ctx.response.body = 'hello world!!!';
});

module.exports = router;
