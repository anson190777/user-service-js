const requestHandlerWrap = require('../middlewares/requestHandlerWrap');
const register = require('../app/functions/register');

module.exports = {
  register: requestHandlerWrap(register),
};
