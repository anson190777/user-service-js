const Result = require('folktale/result');
const mongo = require('../../core/mongo.core');

const COLLECTION_NAME = 'users';

const insertOne = async data => {
  try {
    const db = mongo.db();
    const coll = await db.collection(COLLECTION_NAME);

    const writeResult = await coll.insertOne(data);

    return Promise.resolve(Result.Ok(writeResult.ops[0]));
  } catch (error) {
    return Promise.resolve(Result.Error(error));
  }
};

const findOne = async data => {
  try {
    const db = mongo.db();
    const coll = await db.collection(COLLECTION_NAME);

    const readResult = await coll.findOne(data);

    return Promise.resolve(Result.Ok(readResult));
  } catch (error) {
    return Promise.resolve(Result.Error(error));
  }
};

const findOneAndUpdate = async (filter, update, options) => {
  try {
    const db = mongo.db();
    const coll = await db.collection(COLLECTION_NAME);

    const writeResult = await coll.findOneAndUpdate(filter, update, options);

    return Promise.resolve(Result.Ok(writeResult.value));
  } catch (error) {
    return Promise.resolve(Result.Error(error));
  }
};

module.exports = {
  insertOne,
  findOne,
  findOneAndUpdate,
};
