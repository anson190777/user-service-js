const register = {
  type: 'object',
  required: ['email', 'name', 'password'],
  properties: {
    name: { type: 'string' },
    email: { type: 'string', format: 'email' },
    password: { type: 'string' },
  },
};

module.exports = {
  register,
};
