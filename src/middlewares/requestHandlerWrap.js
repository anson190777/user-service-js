module.exports = fn => async ctx => {
  try {
    const result = await fn(ctx);

    if (result.value instanceof Error) throw result.value;

    ctx.response.status = result.value.status;
    ctx.response.body = result.value.body;
  } catch (error) {
    ctx.throw(error);
  }
};
