const http = require('http');
const config = require('config');
const router = require('./app/router');
const logger = require('./core/logger');
const mongoConf = require('./core/mongo.core');
const app = require('./core/koa.core')(router, logger);

//create server with port 3000
const startApiServer = () =>
  new Promise((resolve, reject) => {
    http.createServer(app.callback()).listen(config.get('port'), err => {
      if (err) return reject(err);
      return resolve();
    });
  });

//start server and connect database
const start = async () => {
  try {
    await startApiServer();

    logger.info(`[MAIN] App is listening on port ${config.get('port')}`);
  } catch (error) {
    logger.error(error);
    process.kill(process.pid, 'SIGTERM');
  }
};

start();

//shutdown server
const shutdown = signal => async err => {
  logger.log(`${signal}...`);
  if (err) logger.error(err.stack || err);
  logger.info(`${signal} signal received.`);
};

process.on('SIGTERM', shutdown('SIGNTERM'));
